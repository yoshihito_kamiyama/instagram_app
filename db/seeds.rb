User.create!(name: "User", user_name: "User", email: "example@railstutorial.org",
             password: "foobar", password_confirmation: "foobar",
             activated: true, activated_at: Time.zone.now)

99.times do |n|
  name = Faker::Name.name
  user_name = "example-#{n + 1}"
  email = "example-#{n + 1}@railstutorial.org"
  password = "password"
  User.create!(name: name, user_name: user_name, email: email,
               password: password, password_confirmation: password,
               activated: true, activated_at: Time.zone.now)
end

all_users = User.all
users = all_users[0..5]
content = Faker::Lorem.sentence(word_count: 5)
5.times do |n|
  users.each { |user| user.photos.create!(content: content,
                                          picture: open("#{Rails.root}/db/fixtures/img#{n + 1}.jpeg"))
             }
end

users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }