class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :user_name
      t.string :email
      t.string :phone_number
      t.string :gender
      t.text :website
      t.text :introduce

      t.timestamps
    end
  end
end
