class FavoritesController < ApplicationController
  before_action :logged_in_user

  def create
    @photo = Photo.find(params[:photo_id])
    @photo.like(current_user)
    redirect_to root_path
  end

  def destroy
    @photo = Favorite.find(params[:id]).photo
    @photo.unlike(current_user)
    redirect_to root_path
  end
end
