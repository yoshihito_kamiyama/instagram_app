class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
                                        :password_edit, :update_password, :following, :followers]
  before_action :correct_user, only: [:edit, :update, :destroy, :password_edit, :update_password]

  def index
    @search = User.ransack(params[:q])
    @users = @search.result.page(params[:page]).per(30)
  end

  def show
    @user = User.find(params[:id])
    @photos = @user.photos.page(params[:page])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render :new
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def password_edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render :edit
    end
  end

  def update_password
    @user = User.find(params[:id])
    if @user.authenticate(params[:user][:password_now])
      @user.update_attributes(user_params)
      flash.now[:success] = "Password changed"
      render :edit
    else
      flash.now[:danger] = "Failure Password change"
      render :password_edit
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:info] = "User deleted"
    redirect_to root_url
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.page(params[:page])
    render "show_follow"
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.page(params[:page])
    render "show_follow"
  end

  def facebook_login
    @user = User.from_omniauth(request.env["omniauth.auth"])
      result = @user.save(context: :facebook_login)
      if result
        log_in @user
        redirect_to @user
      else
        redirect_to auth_failure_path
      end
  end

  def auth_failure
    @user = User.new
    render :new
  end

  private

    def user_params
      params.require(:user).permit(:name, :user_name, :email, :password, :password_confirmation,
                                   :website, :introduce, :phone_number, :gender)
    end

    def correct_password_now?(user)
      user.authenticate(params[:user][:password_now])
    end

    def correct_new_password?
      params[:user][:password] == params[:user][:password_confirmation]
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
