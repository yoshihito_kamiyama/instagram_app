class ApplicationController < ActionController::Base
  before_action :set_search
  include SessionsHelper

  def set_search
    @search = User.ransack(params[:q])
    @users = @search.result.page(params[:page])
  end

  private

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
end
