class PhotosController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :destroy]
  before_action :correct_user, only: :destroy

  def show
    @photo = Photo.find(params[:id])
    @comment = Comment.new
    @comments = @photo.comments
  end

  def new
    @photo = current_user.photos.build
  end

  def create
    @photo = current_user.photos.build(photo_params)
    if @photo.save
      flash[:success] = "Photo posted!"
      redirect_to photo_path(@photo)
    else
      @feed_items = []
      render :new
    end
  end

  def destroy
    @photo.destroy
    flash[:success] = "Post deleted"
    redirect_to request.referrer || root_url
  end

  private

    def photo_params
      params.require(:photo).permit(:content, :picture)
    end

    def correct_user
      @photo = current_user.photos.find_by(id: params[:id])
      redirect_to root_url if @photo.nil?
    end
end
