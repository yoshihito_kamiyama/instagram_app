class StaticPagesController < ApplicationController
  def home
    @feed_items = current_user.feed.page(params[:page]).per(9) if logged_in?
  end

  def terms
  end
end
