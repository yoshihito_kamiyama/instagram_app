class CommentsController < ApplicationController
  before_action :logged_in_user

  def create
    @photo = Photo.find(params[:photo_id])
    @comment = @photo.comments.build(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash[:success] = "Successful comment"
      redirect_back(fallback_location: root_path)
    else
      flash[:danger] = "Failure comment"
      redirect_back(fallback_location: root_path)
    end
  end

  private

    def comment_params
      params.require(:comment).permit(:content)
    end
end
