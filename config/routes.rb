Rails.application.routes.draw do
  root "static_pages#home"
  get "/terms", to: "static_pages#terms"
  get "/signup", to: "users#new"
  post "/signup", to: "users#create"
  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"
  get '/auth/:provider/callback', to: 'users#facebook_login', as: :auth_callback
  get '/auth/failure', to: 'users#auth_failure', as: :auth_failure

  resources :users do
    member do
      get :following, :followers, :password_edit
      patch :password_edit, to: "users#update_password"
    end
  end

  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :photos, only: [:show, :new, :create, :destroy] do
    resources :comments, only: [:create]
  end
  resources :relationships, only: [:create, :destroy]
  resources :favorites, only: [:create, :destroy]
  resources :notifications, only: [:index]
end
