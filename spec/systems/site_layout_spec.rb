require "rails_helper"

RSpec.describe "site layout", type: :system do
  context "access to root path" do
    before { visit root_path }

    # リンクの数のテスト
    it "has links such as root_path and terms_path" do
      expect(page).to have_link nil, href: root_path
      expect(page).to have_link "Terms of service", href: terms_path
    end
  end

  context "access to signup_path" do
    before { visit signup_path }

    # signup_pathにSign upの文字とタイトルがあるかどうか
    it "has 'Sign up' contents and include 'Sign up' at title" do
      expect(page).to have_content "Sign up"
      expect(page).to have_title full_title("Sign up")
    end
  end
end
