require "rails_helper"

RSpec.describe "Edit", type: :system do
  let(:user) { create(:user) }

  # ユーザーは編集に成功する
  it "successful edit" do
    visit user_path(user)
    valid_login(user)
    click_link "Edit profile"
    fill_in "Name", with: "NewName"
    fill_in "User name", with: "hogehoge"
    fill_in "Email", with: "hogehoge@example.com"
    click_button "Save Change"

    expect(page).to have_current_path user_path(user)
    expect(page).to have_selector(".alert-success", text: "Profile updated")
    expect(user.reload.email).to eq "hogehoge@example.com"
  end

  # ユーザーは編集に失敗する
  it "unsuccessful edit" do
    valid_login(user)
    visit user_path(user)
    click_link "Edit profile"
    fill_in "User name", with: ""
    fill_in "Email", with: "foo@invalid"
    click_button "Save Change"

    expect(page).to have_selector(".alert-danger", text: "The form contains")
    expect(user.reload.email).not_to eq "foo@invalid"
  end
end
