require "rails_helper"

RSpec.describe "users", type: :system do
  describe "user create a new account" do
    # 有効な値が入力された時
    context "enter an valid values" do
      before do
        visit signup_path
        fill_in "Name", with: "testuser"
        fill_in "User name", with: "Testuser"
        fill_in "Email", with: "testuser@example.com"
        fill_in "Password", with: "password"
        fill_in "Confirmation", with: "password"
        click_button "Create my account"
      end

      # フラッシュメッセージが出る
      it "gets an flash message" do
        expect(page).to have_selector(".alert-info",
                                      text: "Please check your email to activate your account.")
      end
    end

    # 無効な値が入力された時
    context "enter an invalid values" do
      before do
        visit signup_path
        fill_in "Name", with: ""
        fill_in "User name", with: ""
        fill_in "Email", with: ""
        fill_in "Password", with: ""
        fill_in "Confirmation", with: ""
        click_button "Create my account"
      end

      # エラーの検証
      it "gets an errors" do
        expect(page).to have_selector("#error_explanation")
        expect(page).to have_selector(".alert-danger", text: "The form contains 5 errors.")
        expect(page).to have_content("Password can't be blank", count: 1)
      end
      # 今いるページのURLの検証
      it "render to /signup url" do
        expect(page).to have_current_path '/signup'
      end
    end
  end
end
