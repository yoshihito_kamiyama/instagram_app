require "rails_helper"

RSpec.describe "Following", type: :system do

  let(:user) { create(:user) }
  let(:other_user) { create(:user) }

  shared_examples_for "have user infomation" do
    it { expect(page).to have_css('img.gravatar') }
    it { expect(page).to have_css('h1', text: user.name) }
    it { expect(page).to have_text("Photos") }
    it { expect(page).to have_link("view my profile", href: user_path(user)) }
  end

  describe "following/followers method" do
    describe "following page" do
      before do
        valid_login(user)
        click_link "following"
      end

      it_behaves_like "have user infomation"
    end

    describe "followers page" do
      before do
        valid_login(user)
        click_link "following"
      end

      it_behaves_like "have user infomation"
    end
  end
end
