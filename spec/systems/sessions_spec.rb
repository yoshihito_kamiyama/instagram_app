require "rails_helper"

RSpec.describe "Sessions", type: :system do
  before { visit login_path }

  # 有効な値を入力する
  describe "enter an valid values" do
    let!(:user) { create(:user, email: "loginuser@example.com", password: "password") }

    before do
      fill_in "Email", with: "loginuser@example.com"
      fill_in "Password", with: "password"
      click_button "Log in"
    end

    # ログインした時のページのレイアウトの確認
    it "log in do" do
      expect(page).to have_current_path user_path(user)
      expect(page).not_to have_link nil, href: login_path
      click_link "Account"
      expect(page).to have_link "Profile", href: user_path(user)
      expect(page).to have_link "Log out", href: logout_path
    end
    # ログアウトした時のページのレイアウトを確認
    it "log out after log in" do
      click_link "Account"
      click_link "Log out"
      expect(page).to have_current_path root_path
      expect(page).to have_selector(".alert-success", text: "Successfully logged out")
      expect(page).to have_link "Log in", href: login_path
      expect(page).not_to have_link "Account"
      expect(page).not_to have_link nil, href: logout_path
      expect(page).not_to have_link nil, href: user_path(user)
    end
  end

  # 無効な値を入力する
  describe "enter an invalid values" do
    before do
      fill_in "Email", with: ""
      fill_in "Password", with: ""
      click_button "Log in"
    end

    # フラッシュメッセージが出る
    it "gets an flash messages" do
      expect(page).to have_selector(".alert-danger", text: "Invalid email/password combination")
      expect(page).to have_current_path login_path
    end
    # 違うページにアクセスした時
    context "access to other page" do
      before { visit root_path }

      # フラッシュメッセージが消える
      it "is flash disappear" do
        expect(page).not_to have_selector(".alert-danger",
                                          text: "Invalid email/password combination")
      end
    end
  end
end
