require "rails_helper"

RSpec.describe "User profile", type: :system do
  let(:user) { create(:user) }

  before { user.photos.create(content: "foobar") }

  # プロフィール画面に対するテスト
  it "profile desplay" do
    valid_login(user)
    expect(page).to have_current_path user_path(user)
    expect(page).to have_title full_title(user.name)
    expect(page).to have_selector("li", text: user.name)
    expect(page).to have_selector("li>img")
  end
end
