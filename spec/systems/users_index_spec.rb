require "rails_helper"

RSpec.describe "Index", type: :system do
  let(:user) { create(:user) }

  # 最初のページにユーザーが存在する
  it "user include first page" do
    valid_login(user)
    visit users_path
    expect(page).to have_link user.name, href: user_path(user)
  end
end
