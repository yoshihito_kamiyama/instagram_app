require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    it { expect(full_title("")).to eq "InstagramApp" }
    it { expect(full_title("Terms of service")).to eq "Terms of service | InstagramApp" }
  end
end
