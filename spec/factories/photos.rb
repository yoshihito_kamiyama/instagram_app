FactoryBot.define do
  factory :photo do
    content { "Sample" }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, "spec/fixtures/sample.jpeg")) }
    created_at { Time.zone.now }
    association :user

    trait :yesterday do
      created_at { 1.days.ago }
    end

    trait :year do
      created_at { 1.years.ago }
    end
  end
end
