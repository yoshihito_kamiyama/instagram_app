require "rails_helper"

RSpec.describe "Photos", type: :request do
  let(:photo) { create(:photo) }

  describe "POST #create" do
    # ログインせずに投稿しようとした場合
    context "as a guest" do
      # 投稿はできずにログイン画面にリダイレクトする
      it "redirect create when not logged in" do
        expect { post photos_path, params:
          { photo: { content: "foobar" } }
        }.not_to change(Photo, :count)
        expect(response).to redirect_to login_path
      end
    end
  end

  describe "DELETE #destroy" do
    # ログインせずに削除しようとした場合
    context "as a guest" do
      # 投稿は削除できずにログイン画面にリダイレクトする
      it "redirect destroy when not logged in" do
        photo = create(:photo)
        expect { delete photo_path(photo), params: { id: photo.id } }.not_to change(Photo, :count)
        expect(response).to redirect_to login_path
      end
    end
  end
end
