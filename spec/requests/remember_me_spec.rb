require "rails_helper"

RSpec.describe "Remember me", type: :request do
  let(:user) { create(:user) }

  context "with valid infromation" do
    # ログイン中のみログインできる
    it "logs in with valid information followed by logout" do
      sign_in_as(user)
      expect(response).to redirect_to user_path(user)

      # ログアウトする
      delete logout_path
      expect(response).to redirect_to root_path
      expect(session[:user_id]).to eq nil

      # もう一度ログアウトを行う
      delete logout_path
      expect(response).to redirect_to root_path
      expect(session[:user_id]).to eq nil
    end
  end

  context "authenticated? return false for a user with nil digest" do
    # ダイジェストが存在しない場合のauthenticated?のテスト
    it "is invalid without remember_digest" do
      user = create(:user, remember_digest: nil)
      expect(user.authenticated?(:remember, "")).to be_falsey
    end
  end

  context "login with remembering" do
    it "remember cookies" do
      post login_path, params:
        { session: { email: user.email, password: user.password, remember_me: "1" } }
      expect(response.cookies["remember_token"]).not_to eq nil
    end
  end

  context "login without remembering" do
    it "does not remember cookies" do
      # クッキーを保存してログイン
      post login_path, params:
        { session: { email: user.email, password: user.password, remember_me: "1" } }
      delete logout_path
      # クッキーを保存せずにログイン
      post login_path, params:
        { session: { email: user.email, password: user.password, remember_me: "0" } }
      expect(response.cookies["remember_token"]).to eq nil
    end
  end
end
