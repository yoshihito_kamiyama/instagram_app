require "rails_helper"

RSpec.describe "Photo interface", type: :request do
  let(:user) { create(:user) }
  let(:image_path) { File.join(Rails.root, "spec/fixtures/sample.jpeg") }
  let(:image) { Rack::Test::UploadedFile.new(image_path) }

  it "photo interface" do
    sign_in_as user
    # 無効な送信
    expect { post photos_path, params: { photo: { picture: "" } } }.not_to change(Photo, :count)
    # 有効な送信
    expect {
            post photos_path, params: { content: "sample", photo: { picture: image } }
           }.to change(Photo, :count).by(1)
    expect(response).to redirect_to photo_path(Photo.last.id)
    follow_redirect!
    expect(response.body).to include "sample"
    # 投稿を削除する
    expect { delete photo_path(user) }.to change(Photo, :count).by(-1)
  end
end
