require "rails_helper"

RSpec.describe "relationships", type: :request do
  let(:user) { create(:user) }
  let(:other_user) { create(:user) }
  let(:active) { user.active_relationships.build(followed_id: other_user.id) }

  # 基本的なアクセスに対するテスト
  describe "POST #create" do
    # ログインしていない場合リダイレクトすること
    it "redirects to log in page" do
      expect { post relationships_path }.not_to change(Relationship, :count)
      expect(response).to redirect_to login_path
    end
  end

  describe "DELETE #destroy" do
    # ログインしていな場合リダイレクトすること
    it "redirects to log in page" do
      expect { delete relationship_path(user), params: { id: user } }.not_to change(Relationship, :count)
      expect(response).to redirect_to login_path
    end
  end
end
