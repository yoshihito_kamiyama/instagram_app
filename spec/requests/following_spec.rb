require "rails_helper"

RSpec.describe "following", type: :request do
  let(:user) { create(:user) }
  let(:other_user) { create(:user) }

  before do
    sign_in_as user
  end

  it "follow a user the standard way" do
    expect { post relationships_path, params: { followed_id: other_user.id }
    }.to change(user.following, :count).by(1)
  end

  it "follow a user with Ajax" do
    expect { post relationships_path, xhr: true,
                                      params: { followed_id: other_user.id }
    }.to change(user.following, :count).by(1)
  end

  it "unfollow a user the standard way" do
    user.follow(other_user)
    relationship = user.active_relationships.find_by(followed_id: other_user.id)
    expect { delete relationship_path(relationship) }.to change(user.following, :count).by(-1)
  end

  it "unfollow a user with Ajax" do
    user.follow(other_user)
    relationship = user.active_relationships.find_by(followed_id: other_user.id)
    expect { delete relationship_path(relationship), xhr: true
    }.to change(user.following, :count).by(-1)
  end
end
