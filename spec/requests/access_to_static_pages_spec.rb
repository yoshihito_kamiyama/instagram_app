require "rails_helper"

RSpec.describe "static_pages", type: :request do
  context "GET #home" do
    before { get root_path }

    # 正常にGETリクエストが送られる
    it "responds successfully" do
      expect(response).to have_http_status 200
    end
    # タイトルが正しく表示される
    it "has title 'Instagram'" do
      expect(response.body).to include full_title("")
      expect(response.body).not_to include "| InstagramApp"
    end
  end

  context "GET #terms" do
    before { get terms_path }

    # 正常にリクエストが送られる
    it "responds successfully" do
      expect(response).to have_http_status 200
    end
    # タイトルが正しく表示される
    it "has title 'Terms of service | InstagramApp'" do
      expect(response.body).to include full_title("Terms of service")
    end
  end
end
