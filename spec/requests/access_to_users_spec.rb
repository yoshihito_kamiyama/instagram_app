require "rails_helper"

RSpec.describe "access to users", type: :request do
  let(:user) { create(:user) }
  let(:other_user) { create(:user) }

  describe "GET #index" do
    # 認可されたユーザーとして
    context "as an authorized user" do
      it "responds successfully" do
        sign_in_as user
        get users_path
        expect(response).to be_successful
        expect(response).to have_http_status 200
      end
    end

    # ログインしていないユーザーの場合
    context "as a guest" do
      # ログイン画面にリダイレクトすること
      it "redirects to the login page" do
        get users_path
        expect(response).to have_http_status 302
        expect(response).to redirect_to login_path
      end
    end
  end

  describe "GET #new" do
    # 正常にGETリクエストが送られる
    it "responds successfully" do
      get signup_path
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end
  end

  describe "#create" do
    include ActiveJob::TestHelper

    it "is invalid with invalid signup information" do
      perform_enqueued_jobs do
        expect {
          post users_path,  params:
            { user: { name: "", user_name: "", email: "user@invalid",
                      password: "foo", password_confirmation: "bar" } }
        }.not_to change(User, :count)
      end
    end

    it "is valid with valid signup information" do
      perform_enqueued_jobs do
        expect {
          post users_path, params:
            { user: { name: "ExampleUser", user_name: "ExampleUser",
                      email: "user@example.com", password: "password",
                      password_confirmation: "password" } }
        }.to change(User, :count).by(1)

        expect(response).to redirect_to root_path
        user = assigns(:user)
        # 有効化していない状態でログインしてみる
        sign_in_as(user)
        expect(session[:user_id]).to be_falsey
        # 有効化トークンが不正な場合
        get edit_account_activation_path("invalid token", email: user.email)
        expect(session[:user_id]).to be_falsey
        # トークンは正しいがメールアドレスが無効な場合
        get edit_account_activation_path(user.activation_token, email: "wrong")
        expect(session[:user_id]).to be_falsey
        # 有効化トークンが正しい場合
        get edit_account_activation_path(user.activation_token, email: user.email)
        expect(session[:user_id]).to eq user.id
        expect(user.name).to eq "ExampleUser"
        expect(user.user_name).to eq "ExampleUser"
        expect(user.email).to eq "user@example.com"
        expect(user.password).to eq "password"
      end
    end

    # 無効なリクエスト
    context "invalid request" do
      # 無効なユーザーデータを作成
      let(:user_params) do
        attributes_for(:user, name: "", user_name: "", email: "user@invalid",
                              password: "password", password_confirmation: "password")
      end

      # ユーザーが追加されない
      it "does not add a user" do
        expect do
          post signup_path, params: { user: user_params }
        end.to change(User, :count).by(0)
      end
    end
  end

  describe "GET #edit" do
    # 認可されたユーザーとして
    context "as an authorized user" do
      it "responds successfully" do
        sign_in_as user
        get edit_user_path(user)
        expect(response).to be_successful
        expect(response).to have_http_status 200
      end
    end

    # ログインしていないユーザーの場合
    context "as a guest" do
      # ログイン画面にリダイレクトする
      it "redirects to the login page" do
        get edit_user_path(user)
        expect(response).to have_http_status 302
        expect(response).to redirect_to login_path
      end
    end

    # アカウントが違うユーザーの場合
    context "as other user" do
      # ホーム画面にリダイレクトすること
      it "redirects to the login path" do
        sign_in_as other_user
        get edit_user_path(user)
        expect(response).to redirect_to root_path
      end
    end
  end

  describe "PATCH #update" do
    # 認可されたユーザーとして
    context "as an authorized user" do
      # ユーザーを更新できること
      it "updates a user" do
        user_params = attributes_for(:user, name: "NewName")
        sign_in_as user
        patch user_path(user), params: { id: user.id, user: user_params }
        expect(user.reload.name).to eq "NewName"
      end
    end

    # ログインしていないユーザーの場合
    context "as a guest" do
      # ログイン画面にリダイレクトする
      it "redirects to the login page" do
        user_params = attributes_for(:user, name: "NewName")
        patch user_path(user), params: { id: user.id, user: user_params }
        expect(response).to have_http_status 302
        expect(response).to redirect_to login_path
      end
    end

    # アカウントが違うユーザーの場合
    context "as other user" do
      # ユーザーを更新できない
      it "does not update the user" do
        user_params = attributes_for(:user, name: "NewName")
        sign_in_as other_user
        patch user_path(user), params: { id: user.id, user: user_params }
        expect(user.reload.name).to eq other_user.name
      end
      # ホーム画面にリダイレクトする
      it "redirects to the login page" do
        user_params = attributes_for(:user, name: "NewName")
        sign_in_as other_user
        patch user_path(user), params: { id: user.id, user: user_params }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe "DELETE #destroy" do
    # 認可されたユーザーとして
    context "as an authorized user" do
      # ユーザーを削除できること
      it "deletes a user" do
        sign_in_as user
        expect { delete user_path(user), params: { id: user.id } }.to change(User, :count).by(-1)
      end
    end

    # ログインしていない場合
    context "as a guest" do
      # ログイン画面にリダイレクトする
      it "redirects to the log in page" do
        delete user_path(user), params: { id: user.id }
        expect(response).to have_http_status 302
        expect(response).to redirect_to login_path
      end
    end

    # アカウントが違うユーザーの場合
    context "as an authorized user" do
      # ホーム画面にリダイレクトする
      it "redirects to the home page" do
        sign_in_as other_user
        delete user_path(user), params: { id: user.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe "#GET follow/followed" do
    # ログインしていない場合
    context "as a guest" do
      # ログイン画面にリダイレクトする
      it "following redirects to the log in page" do
        get following_user_path(user)
        expect(response).to redirect_to login_path
      end
      it "followers redirectes to the log in page" do
        get followers_user_path(user)
        expect(response).to redirect_to login_path
      end
    end
  end
end
