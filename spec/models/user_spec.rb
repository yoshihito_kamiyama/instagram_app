require 'rails_helper'

RSpec.describe User, type: :model do
  # FactoryBotが存在するかのテスト
  it "has a valid factory bot" do
    expect(build(:user)).to be_valid
  end

  # Shoulda Matchersのvalidationsテスト
  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(50) }
    it { is_expected.to validate_presence_of(:user_name) }
    it { is_expected.to validate_length_of(:user_name).is_at_most(50) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_length_of(:email).is_at_most(255) }
    it { is_expected.to validate_presence_of(:password) }
    it { is_expected.to validate_length_of(:password).is_at_least(6) }

    # user_nameの一意性の検証
    it "validate uniqueness of user_name" do
      create(:user, user_name: "original")
      user = build(:user, user_name: "original")
      expect(user).not_to be_valid
    end

    # メールのフォーマットの検証
    context "email format" do
      # 有効なメールアドレス
      it "valid email" do
        is_expected.to allow_values("first.last@foo.jp", "user@exmaple.com", "USER@foo.COM",
                                    "A_US-ER@foo.bar.org", "alice+bob@baz.cn").for(:email)
      end
      # 無効なメールアドレス
      it "invalid email" do
        is_expected.not_to allow_values("user@example,com", "user_at_foo.org", "user.@example.",
                                        "foo@bar_baz.com", "foo@bar+baz.com").for(:email)
      end
    end

    # メールアドレスの一意性の検証
    describe "validate uniqueness of email" do
      let!(:user) { create(:user, email: "original@example.com") }

      # 重複するメールアドレスは拒否する
      it "is invalid with a duplicate email" do
        user = build(:user, email: "original@example.com")
        expect(user).not_to be_valid
      end
      # 小文字と大文字を区別しない
      it "is case insensitive in email" do
        user = build(:user, email: "ORIGINAL@EXAMPLE.COM")
        expect(user).not_to be_valid
      end
    end
  end

  # メールアドレスのbefore_saveの検証
  describe "before_save" do
    describe "#email_downcase" do
      let!(:user) { create(:user, email: "ORIGINAL@EXAMPLE.COM") }

      it "makes email to low case" do
        expect(user.reload.email).to eq "original@example.com"
      end
    end
  end

  # passwordとpassword_confirmationが一致するべきの検証
  describe "when password does not match confirmation" do
    # 一致する場合
    it "is valid when password comfirmation matches password" do
      user = build(:user, password: "password", password_confirmation: "password")
      expect(user).to be_valid
    end
    # 一致しない場合
    it "is invalid when password confirmation does not match password" do
      user = build(:user, password: "password", password_confirmation: "different")
      expect(user).not_to be_valid
    end
  end
end
