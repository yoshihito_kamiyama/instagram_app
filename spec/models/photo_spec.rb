require 'rails_helper'

RSpec.describe Photo, type: :model do
  let(:photo) { create(:photo) }

  # FactoryBotが存在するかのテスト
  it "has a valid factory bot" do
    expect(photo).to be_valid
  end

  # UserモデルとPhotoモデルは必ず関連付けられているか
  it "Photo model is associated User model" do
    photo = build(:photo, user_id: nil)
    expect(photo).not_to be_valid
  end

  # 空のcontentは禁止にする
  it "Photo content is presence of ture" do
    photo = build(:photo, content: "")
    expect(photo).not_to be_valid
  end

  # contentは140文字以内にする
  it "Photo content is at most 140 characters" do
    photo = build(:photo, content: "a" * 141)
    expect(photo).not_to be_valid
  end

  # 順序付けのテスト
  it "order is recent first" do
    create(:photo, :year)
    create(:photo, :yesterday)
    expect(photo).to eq Photo.first
  end

  # ユーザーが削除されたらPhotoも削除される
  it "photo is relative to user destroyed" do
    photo = create(:photo)
    expect { photo.destroy }.to change(Photo, :count).by(-1)
  end
end
