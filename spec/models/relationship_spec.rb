require 'rails_helper'

RSpec.describe Relationship, type: :model do
  let(:user) { create(:user) }
  let(:other_user) { create(:user) }
  let(:active) { user.active_relationships.build(followed_id: other_user.id) }

  # FactoryBotが存在するかのテスト
  it "has a valid factory bot" do
    expect(active).to be_valid
  end

  # validationテスト
  describe "validations" do
    describe "presence" do
      it { is_expected.to validate_presence_of :followed_id }
      it { is_expected.to validate_presence_of :follower_id }
    end
  end

  # follow/followedメソッドのテスト
  describe "follower/followed methods" do
    it { should respond_to(:follower) }
    it { should respond_to(:followed) }
    # followメソッドはフォローしているユーザーを返すこと
    it "follower method return following-user" do
      expect(active.follower).to eq user
    end
    # followerメソッドはフォローされているユーザーを返すこと
    it "followed method retuen followed-user" do
      expect(active.followed).to eq other_user
    end
  end
end
