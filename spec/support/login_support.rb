module LoginSupport
  # ログインする
  def valid_login(user)
    visit root_path
    click_link "Log in"
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Log in"
  end

  # ログインしているかどうか
  def is_logged_in?
    !session[:user_id].nil?
  end
end
